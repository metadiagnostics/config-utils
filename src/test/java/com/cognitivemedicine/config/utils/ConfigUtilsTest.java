/*******************************************************************************
 *  Copyright (c) 2016, 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *  
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *  
 *        http://www.apache.org/licenses/LICENSE-2.0
 *  
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *******************************************************************************/
package com.cognitivemedicine.config.utils;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.configuration.ConfigurationException;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Note: All major variants except the use com.cognitivemedicine.config.commondirectory as an environment variable are tested. 
 *
 * @author esteban & jerry goodnough
 */
public class ConfigUtilsTest {
    
    private static final Logger LOG = LoggerFactory.getLogger(ConfigUtils.class);

    @Before
    public void doBefore(){
        //Let's start with no env variables.
        setEnv(new HashMap());
        //and a clean cache
        ConfigUtils.clearConfigUtilsInstancesCache();
    }
    
    @After
    public void doAfter(){
        //Let's start with no env variables.
        setEnv(new HashMap());
        //and a clean cache
        ConfigUtils.clearConfigUtilsInstancesCache();
    }
    
    @Test
    public void contextsTest() throws ConfigurationException {
        LOG.debug("contextsTest");
        
        String contextA = "context-a";
        String contextB = "context-b";
        String contextC = "context-c";

        ConfigUtils ca = ConfigUtils.getInstance(contextA);
        ConfigUtils cb = ConfigUtils.getInstance(contextB);
        ConfigUtils cc = ConfigUtils.getInstance(contextC);

        assertThat(ca.getString("prop1"), is("A1"));
        assertThat(ca.getString("prop2"), is("A2"));

        assertThat(cb.getString("prop1"), is("B1"));
        assertThat(cb.getString("prop2"), is("B2"));

        //If no context is found, ConfigUtils will fall back to the env variables
        //meaning that no error will be thrown, but no variables will exist (other
        //than the env variables from the OS)
        assertThat(cc.getString("missingprop1"), is(nullValue()));
        assertThat(cc.getString("missingprop2"), is(nullValue()));
    }

    @Test
    public void envTests() throws ConfigurationException {
        LOG.debug("envTests");
        Map<String, String> env = new HashMap<>();
        env.put("context-c.prop1", "C1");
        env.put("context-c.prop2", "C2");
        setEnv(env);

        String contextA = "context-a";
        String contextC = "context-c";

        ConfigUtils ca = ConfigUtils.getInstance(contextA);
        ConfigUtils cc = ConfigUtils.getInstance(contextC);

        assertThat(ca.getString("prop1"), is("A1"));
        assertThat(ca.getString("prop2"), is("A2"));
        assertThat(ca.getString("prop3"), is("A3"));
        assertThat(ca.getString("prop4"), is("A4"));

        //Env variables are accessed without the prefix part ('context-c.').
        assertThat(cc.getString("prop1"), is("C1"));
        assertThat(cc.getString("prop2"), is("C2"));
    }
    
    @Test
    public void overwriteTests() throws ConfigurationException {
        LOG.debug("overwriteTests");
        Map<String, String> env = new HashMap<>();
        env.put("context-a.prop3", "C1");
        env.put("context-a.prop4", "C2");
        setEnv(env);

        String contextA = "context-a";

        ConfigUtils ca = ConfigUtils.getInstance(contextA);

        assertThat(ca.getString("prop3"), is("C1"));
        assertThat(ca.getString("prop4"), is("C2"));
        //Cleanup
        env.remove("context-a.prop3");
        env.remove("context-a.prop4");
        setEnv(env);
        ConfigUtils.clearConfigUtilsInstancesCache();
        

    }
    
    @Test
    public void commonSysOverwriteTest() throws ConfigurationException 
    {
        LOG.debug("commonSysOverwriteTests");
        ConfigUtils.clearConfigUtilsInstancesCache();
    	String sysVar = "com.cognitivemedicine.config.commondirectory";
    	String testDir = findCommonDirectory();

    	System.setProperty(sysVar,testDir);
        
        String contextA = "context-a";

        ConfigUtils ca = ConfigUtils.getInstance(contextA);

        assertThat(ca.getString("prop1"), is("CMA1"));
        assertThat(ca.getString("prop2"), is("CMA2"));
        assertThat(ca.getString("prop3"), is("A3"));
        assertThat(ca.getString("prop4"), is("A4"));
        assertThat(ca.getString("prop5"), is("A5"));
        assertThat(ca.getString("prop6"), is("A6"));
        
        System.clearProperty(sysVar);
        
        ConfigUtils.clearConfigUtilsInstancesCache();
        
    }
    
    @Test
    public void commonEnvOverwriteTest() throws ConfigurationException 
    {
        LOG.debug("commonEnvOverwriteTests");
        ConfigUtils.clearConfigUtilsInstancesCache();
    	String envVar = "com_cognitivemedicine_config_commondirectory";
    	String testDir = findCommonDirectory();
        
    	Map<String, String> env = new HashMap<>();
    	env.put(envVar,testDir);
    	env.put("context-a.prop3", "C1");
        env.put("context-a.prop4", "C2");
        setEnv(env);  
        String contextA = "context-a";

        ConfigUtils ca = ConfigUtils.getInstance(contextA);

        assertThat(ca.getString("prop1"), is("CMA1"));
        assertThat(ca.getString("prop2"), is("CMA2"));
        assertThat(ca.getString("prop3"), is("C1"));
        assertThat(ca.getString("prop4"), is("C2"));
        assertThat(ca.getString("prop5"), is("A5"));
        assertThat(ca.getString("prop6"), is("A6"));
        
        env.remove(envVar);
    	env.remove("context-a.prop3");
        env.remove("context-a.prop4");
        setEnv(env);  
  
        ConfigUtils.clearConfigUtilsInstancesCache();
        
    }


    /**
     * Nasty way to set env variables after an app is running.
     * @param newenv 
     */
    private static void setEnv(Map<String, String> newenv) {
        try {
            Class<?> processEnvironmentClass = Class.forName("java.lang.ProcessEnvironment");
            Field theEnvironmentField = processEnvironmentClass.getDeclaredField("theEnvironment");
            theEnvironmentField.setAccessible(true);
            Map<String, String> env = (Map<String, String>) theEnvironmentField.get(null);
            env.putAll(newenv);
            Field theCaseInsensitiveEnvironmentField = processEnvironmentClass.getDeclaredField("theCaseInsensitiveEnvironment");
            theCaseInsensitiveEnvironmentField.setAccessible(true);
            Map<String, String> cienv = (Map<String, String>) theCaseInsensitiveEnvironmentField.get(null);
            cienv.putAll(newenv);
        } catch (NoSuchFieldException e) {
            try {
                Class[] classes = Collections.class.getDeclaredClasses();
                Map<String, String> env = System.getenv();
                for (Class cl : classes) {
                    if ("java.util.Collections$UnmodifiableMap".equals(cl.getName())) {
                        Field field = cl.getDeclaredField("m");
                        field.setAccessible(true);
                        Object obj = field.get(env);
                        Map<String, String> map = (Map<String, String>) obj;
                        map.clear();
                        map.putAll(newenv);
                    }
                }
            } catch (SecurityException | NoSuchFieldException | IllegalArgumentException | IllegalAccessException e2) {
                LOG.error("Exception trying to set env variables", e2);
            }
        } catch (ClassNotFoundException | SecurityException | IllegalArgumentException | IllegalAccessException e1) {
            LOG.error("Exception trying to set env variables", e1);
        }
    }
    
    private static String findCommonDirectory()
    {
    	String dir;
    	String context = "context-a.properties";
    	//Find the path to context-a.properties
    	//Strip off the end and add "commontest"
    	String configResourceName = "/" + context;
    	URL rsc = ConfigUtilsTest.class.getResource(configResourceName);
    	
        if (System.getProperty("os.name").toLowerCase().contains("win")){
            dir = rsc.getPath().substring(1);
            dir = dir.substring(0, dir.length()-context.length()) + "commontest";
        } else {
            dir = rsc.getPath().substring(0, rsc.getPath().length()-context.length()) + "commontest";
        }
        
    	return dir;
    }
    
    

}
