/*******************************************************************************
 *  Copyright (c) 2016, 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *  
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *  
 *        http://www.apache.org/licenses/LICENSE-2.0
 *  
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *******************************************************************************/
package com.cognitivemedicine.config.utils;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.StreamSupport;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.EnvironmentConfiguration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.SystemConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class used to access the configuration properties for a context. The
 * configuration properties contained by this class are first looked up into a
 * properties file with the same name of the context. Then, the environment
 * variables are scanned looking for any property for this context. The
 * environment variables will overwrite any pre-existing property from the
 * properties file. TODO: Fix this description and add info about default
 * directories etc...
 * 
 * @author esteban
 */
public class ConfigUtils {

	private static final Logger LOG = LoggerFactory.getLogger(ConfigUtils.class);

	private static Map<String, ConfigUtils> INSTANCES = Collections.synchronizedMap(new HashMap<>());

	private final Configuration baseConfiguration;

	private static String commonDirectory=null;

	public static String getCommonDirectory() {
		return commonDirectory;
	}

	public static void setCommonDirectory(String directory) {
		commonDirectory = directory;
	}

	private static void defaultCommonDirectory() {
		java.nio.file.Path path;
		String definedEnv = System.getProperty("com.cognitivemedicine.config.commondirectory");
		if (definedEnv != null) {
			path = java.nio.file.Paths.get(definedEnv);
			LOG.debug(
					"Using common configuration directory specified by system propety com.cognitivemedicine.config.commondirectory");
		}
		else {
			definedEnv = System.getenv("com_cognitivemedicine_config_commondirectory");
			if (definedEnv != null) {
				path = java.nio.file.Paths.get(definedEnv);
				LOG.debug(
						"Using common configuration directory specified by environment variable com.cognitivemedicine.config.commondirectory");
			}
			else
			{
				String home = System.getProperty("user.home");
				path = java.nio.file.Paths.get(home, "cognitivemedicine", "config");
				LOG.debug("Using default common configuration directory");
			}
		}
		commonDirectory = path.toString();
		LOG.debug("Common configuration directory is " + commonDirectory);
		if (!java.nio.file.Files.exists(path)) {
			LOG.info("Configuration common directory [" + commonDirectory + "] does not exist");
		}
	}

	/**
	 * Returns the ConfigUtils associated to a context. If a ConfigUtils was
	 * already asked for a context, the cached ConfigUtils is returned.
	 * 
	 * The initialization of a ConfigUtils will scan number of places building a
	 * composite configuration that acts is a override manner.
	 * 
	 * The priority order is: System Variables with the prefix "${context"}
	 * Environment Variables with the prefix "${context}" Properties from the
	 * context file would in the common configuration directory (if found).
	 * Properties from the context file found in the root level of the class
	 * path (if found.
	 * 
	 * The context files is a properties file with the name
	 * "${context}.properties".
	 * 
	 * By default the common do configuration directory is:
	 * ${user.home}/cognitivemedicine/config. This default may be overridden by
	 * setting the environment variable:
	 * com.cognitivemedicine.config.commondirectory
	 *
	 * @param context
	 * @return
	 * @throws ConfigurationException
	 */
	public static synchronized ConfigUtils getInstance(String context) {
		LOG.debug("Retrieving configurations for context '{}'.", context);
		ConfigUtils conf = INSTANCES.get(context);
		if (commonDirectory == null)
		{
			defaultCommonDirectory();
		}
		if (conf == null) {
			LOG.debug("Configuration for context '{}' is not yet loaded.", context);

			// Get the environment variable configuration

			// Because when dealing with system variables we have to
			// add a prefix to each search, we just create a copy of the
			// variables removing the context the prefix from their keys.
			// This will allow, for example, to retrieve the system property
			// 'app1.database.url' as 'database.url' for the context 'app1'.
			LOG.debug("Looking for any configuration property for context '{}' in the environment variables.", context);
			Configuration environmentConfiguration = new EnvironmentConfiguration().subset(context);
			if (LOG.isTraceEnabled()) {
				Iterable<String> iterable = () -> environmentConfiguration.getKeys();
				long size = StreamSupport.stream(iterable.spliterator(), false).count();
				LOG.debug("{} configuration properties found in the environmental variables for context '{}'.", size,
						context);
			}

			// Check for the context in the common directory to allow overrides
			PropertiesConfiguration commonProperties = null;
			java.nio.file.Path commonFile = java.nio.file.Paths.get(commonDirectory, context + ".properties");
			if (java.nio.file.Files.exists(commonFile)) {
				try {
					commonProperties = new PropertiesConfiguration(commonFile.toFile());
				} catch (org.apache.commons.configuration.ConfigurationException ex) {
					String errorMessage = String.format(
							"Exception loading common configuration properties for context '%s' from %s: %s", context,
							commonFile, ex.getMessage());
					LOG.error(errorMessage, ex);
					throw new com.cognitivemedicine.config.utils.ConfigurationException(errorMessage);
				}
				if (LOG.isTraceEnabled()) {
					Iterable<String> iterable = () -> environmentConfiguration.getKeys();
					long size = StreamSupport.stream(iterable.spliterator(), false).count();
					LOG.debug("{} configuration properties found in the common directory for context '{}'.", size,
							context);
				}

			}

			PropertiesConfiguration pathProperties = null;
			// First let see if the corresponding file is in the site specific
			// configuration directory
			// First let's look for the corresponding file in the classpath.
			String configResourceName = "/" + context + ".properties";
			InputStream configFileAsStream = ConfigUtils.class.getResourceAsStream(configResourceName);
			if (configFileAsStream != null) {
				try {
					LOG.debug("Configuration file for context '{}' found in '{}'. Loading properties from it.", context,
							configResourceName);
					pathProperties = new PropertiesConfiguration();
					pathProperties.load(new InputStreamReader(configFileAsStream));
					if (LOG.isTraceEnabled()) {
						Iterable<String> iterable = () -> environmentConfiguration.getKeys();
						long size = StreamSupport.stream(iterable.spliterator(), false).count();
						LOG.debug("{} configuration properties found in the path for context '{}'.", size,
								context);
					}
				} catch (org.apache.commons.configuration.ConfigurationException ex) {
					String errorMessage = String.format(
							"Exception loading configuration properties for context '%s' from %s: %s", context,
							configResourceName, ex.getMessage());
					LOG.error(errorMessage, ex);
					throw new com.cognitivemedicine.config.utils.ConfigurationException(errorMessage);
				}
			} else {
				LOG.debug("Configuration file for context '{}' NOT found in '{}'.", context, configResourceName);
			}

			LOG.debug("Looking for any configuration property for context '{}' in the system properties.", context);
			Configuration systemConfiguration = new SystemConfiguration().subset(context);

			if (LOG.isTraceEnabled()) {
				Iterable<String> iterable = () -> systemConfiguration.getKeys();
				long size = StreamSupport.stream(iterable.spliterator(), false).count();
				LOG.debug("{} configuration properties found in the system properties for context '{}'.", size,
						context);
			}

			// TODO: Consider adding the common properties (if present) to
			// constructor of the composite to hold saved changes.

			CompositeConfiguration cc = new CompositeConfiguration();

			// Assemble the configurations in preferred order
			// No matter if a specific configuration file was found, we always
			// check for the environment variables so we have the chance to
			// override
			// any property defined in a file.

			cc.addConfiguration(systemConfiguration);

			cc.addConfiguration(environmentConfiguration);
			if (commonProperties != null) {
				cc.addConfiguration(commonProperties);
			}
			if (pathProperties != null) {
				cc.addConfiguration(pathProperties);
			}

			// Dump state
			if (LOG.isTraceEnabled()) {
				LOG.trace("Found Properties:");
				cc.getKeys().forEachRemaining(k -> {
					LOG.trace("****** {} -> {}", k, cc.getProperty(k));
				});
			}
			conf = new ConfigUtils(cc);
			INSTANCES.put(context, conf);
		} else {
			LOG.debug("Cached Configuration Properties Context {} are being used.", context);
		}

		return conf;
	}

	/**
	 * Clears the cache of already initialized ConfigUtils.
	 */
	public static synchronized void clearConfigUtilsInstancesCache() {
		INSTANCES = new HashMap<>();
		commonDirectory=null;
	}

	private ConfigUtils(Configuration baseConfiguration) {
		this.baseConfiguration = baseConfiguration;
	}

	public Configuration subset(String prefix) {
		return baseConfiguration.subset(prefix);
	}

	public boolean isEmpty() {
		return baseConfiguration.isEmpty();
	}

	public boolean containsKey(String key) {
		return baseConfiguration.containsKey(key);
	}

	public Iterator<String> getKeys(String prefix) {
		return baseConfiguration.getKeys(prefix);
	}

	public Iterator<String> getKeys() {
		return baseConfiguration.getKeys();
	}

	public boolean getBoolean(String key) {
		return baseConfiguration.getBoolean(key);
	}

	public boolean getBoolean(String key, boolean defaultValue) {
		return baseConfiguration.getBoolean(key, defaultValue);
	}

	public Boolean getBoolean(String key, Boolean defaultValue) {
		return baseConfiguration.getBoolean(key, defaultValue);
	}

	public byte getByte(String key) {
		return baseConfiguration.getByte(key);
	}

	public byte getByte(String key, byte defaultValue) {
		return baseConfiguration.getByte(key, defaultValue);
	}

	public Byte getByte(String key, Byte defaultValue) {
		return baseConfiguration.getByte(key, defaultValue);
	}

	public double getDouble(String key) {
		return baseConfiguration.getDouble(key);
	}

	public double getDouble(String key, double defaultValue) {
		return baseConfiguration.getDouble(key, defaultValue);
	}

	public Double getDouble(String key, Double defaultValue) {
		return baseConfiguration.getDouble(key, defaultValue);
	}

	public float getFloat(String key) {
		return baseConfiguration.getFloat(key);
	}

	public float getFloat(String key, float defaultValue) {
		return baseConfiguration.getFloat(key, defaultValue);
	}

	public Float getFloat(String key, Float defaultValue) {
		return baseConfiguration.getFloat(key, defaultValue);
	}

	public int getInt(String key) {
		return baseConfiguration.getInt(key);
	}

	public int getInt(String key, int defaultValue) {
		return baseConfiguration.getInt(key, defaultValue);
	}

	public Integer getInteger(String key, Integer defaultValue) {
		return baseConfiguration.getInteger(key, defaultValue);
	}

	public long getLong(String key) {
		return baseConfiguration.getLong(key);
	}

	public long getLong(String key, long defaultValue) {
		return baseConfiguration.getLong(key, defaultValue);
	}

	public Long getLong(String key, Long defaultValue) {
		return baseConfiguration.getLong(key, defaultValue);
	}

	public short getShort(String key) {
		return baseConfiguration.getShort(key);
	}

	public short getShort(String key, short defaultValue) {
		return baseConfiguration.getShort(key, defaultValue);
	}

	public Short getShort(String key, Short defaultValue) {
		return baseConfiguration.getShort(key, defaultValue);
	}

	public BigDecimal getBigDecimal(String key) {
		return baseConfiguration.getBigDecimal(key);
	}

	public BigDecimal getBigDecimal(String key, BigDecimal defaultValue) {
		return baseConfiguration.getBigDecimal(key, defaultValue);
	}

	public BigInteger getBigInteger(String key) {
		return baseConfiguration.getBigInteger(key);
	}

	public BigInteger getBigInteger(String key, BigInteger defaultValue) {
		return baseConfiguration.getBigInteger(key, defaultValue);
	}

	public String getString(String key) {
		return baseConfiguration.getString(key);
	}

	public String getString(String key, String defaultValue) {
		return baseConfiguration.getString(key, defaultValue);
	}

	public String[] getStringArray(String key) {
		return baseConfiguration.getStringArray(key);
	}

	public List<Object> getList(String key) {
		return baseConfiguration.getList(key);
	}

	public List<Object> getList(String key, List<?> defaultValue) {
		return baseConfiguration.getList(key, defaultValue);
	}

	public void setProperty(String key, String value) {
		baseConfiguration.setProperty(key, value);
	}

}
